#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sqlite3
import logging
import pandas as pd

plogger = logging.getLogger("pipeline.db")

# local imports
from pathsandconsts import *

conn = sqlite3.connect(DATABASE)


def dateDB2int(date):
	"""
	converts date from 23/03/16 to 20160323
	:param date: date in format above
	"""
	if not date:
		return None
	i = date.split("/")
	if len(i) != 3:
		return None
	return int("20" + i[2] + i[1] + i[0])


def create():
	"""
	Create empty DB tables
	"""
	with conn:
		c = conn.cursor()
		# c.execute("DROP TABLE users")
		# c.execute("DROP TABLE logs")
		c.execute('''CREATE TABLE users (id INTEGER PRIMARY KEY, user TEXT, glasses TEXT, from_date INT, to_date INT)''')
		c.execute('''CREATE TABLE logs (id INTEGER PRIMARY KEY, date TEXT, glasses TEXT, user TEXT,
						log_time INT, gps_time INT, map_path TEXT,
						sensor_walking_time INT, sensor_stationary_time INT,
						sensor_othermove_time INT, sensor_playing_time INT)''')
		plogger.info("Created tables users and logs.")


def write(table, glasses, date, values, force=False):
	"""
	Write to DB
	"""
	# decide what to write
	to_write = {}
	for k, v in values.iteritems():
		if exists_log(table, glasses, date, k) and not force:
			plogger.info("Already set value %s:%s in %s %s %s" % (k, v, table, glasses, date))
		else:
			to_write[k] = v

	if len(to_write.keys()) > 0:
		# only update the values
		if exists_log(table, glasses, date, "id"):
			with conn:
				c = conn.cursor()
				for k, v in to_write.iteritems():
					c.execute("UPDATE %s SET %s='%s' WHERE glasses='%s' AND date='%s'" % (table, k, v, glasses, date))
					plogger.debug(
						"UPDATE %s SET %s='%s' WHERE glasses='%s' AND date='%s'" % (table, k, v, glasses, date))
				conn.commit()

		# new log
		else:
			with conn:
				c = conn.cursor()
				keys = ",".join(to_write.keys())
				vals = "','".join([str(to_write[k]) for k in to_write.keys()])
				c.execute("INSERT INTO %s(date, glasses, %s) VALUES ('%s','%s','%s')" % (table, keys, date, glasses, vals))
				plogger.debug("INSERT INTO %s(date, glasses, %s) VALUES ('%s','%s','%s')" % (table, keys, date, glasses, vals))
				conn.commit()


def write_log(glasses, date, values, force=False):
	"""
	Write values to log table
	"""
	write("logs", glasses, date, values, force=force)


def write_user(values):
	"""
	Write one row to user table
	"""
	with conn:
		c = conn.cursor()
		keys = ",".join(values.keys())
		vals = "','".join([str(values[k]) for k in values.keys()])
		c.execute("INSERT INTO users(%s) VALUES ('%s')" % (keys, vals))
		plogger.debug("INSERT INTO users(%s) VALUES ('%s')" % (keys, vals))
		conn.commit()


def write_contact(values):
	"""
	Write one row to user table
	"""
	with conn:
		c = conn.cursor()
		keys = ",".join(values.keys())
		vals = "','".join([str(values[k]) for k in values.keys()])
		c.execute("INSERT INTO contacts(%s) VALUES ('%s')" % (keys, vals))
		plogger.debug("INSERT INTO contacts(%s) VALUES ('%s')" % (keys, vals))
		conn.commit()


def dump(table, ):
	"""
	Dump TABLE to python list
	"""
	with conn:
		c = conn.cursor()
		out = []
		for row in c.execute("SELECT * FROM %s" % table):
			out.append(row)
	return out


def exists_log(table, glasses, date, key):
	"""
	Check whether exists a value in TABLE
	"""
	with conn:
		c = conn.cursor()
		out = []
		plogger.debug("SELECT (%s) FROM (%s) WHERE glasses=%s AND date=%s" % (key, table, glasses, date))
		for row in c.execute("SELECT (%s) FROM (%s) WHERE glasses='%s' AND date='%s'" % (key, table, glasses, date)):
			if row[0]:
				out.append(row)
	return len(out) > 0


def read_csv_to_db_users(csvfile):
	"""
	Read csv file into users table
	:param csvfile:
	:return:
	"""
	df = pd.read_csv(csvfile)
	# clean the table
	with conn:
		c = conn.cursor()
		c.execute("DROP TABLE IF EXISTS users")
		c.execute('''CREATE TABLE users (id INTEGER PRIMARY KEY, user TEXT, glasses TEXT, from_date INT, to_date INT)''')
	# fill in form csv
	for i, row in df.iterrows():
		write_user(dict(row))


def read_csv_to_db_contacts(csvfile):
	"""
	Read csv file into users table
	:param csvfile:
	:return:
	"""
	df = pd.read_csv(csvfile)
	# clean the table
	with conn:
		c = conn.cursor()
		c.execute("DROP TABLE IF EXISTS contacts")
		c.execute('''CREATE TABLE contacts (id INTEGER PRIMARY KEY, user TEXT, email TEXT, send_until TEXT)''')
	# fill in form csv
	for i, row in df.iterrows():
		write_contact(dict(row))


def read(table, key, filter_vals=None):
	"""
	Read from table in DB all values of KEY, opt use filter as dict {"key": "filt_val"}
	KEY could be as list ["val1", "val2"] or single string value
	"""
	out = []
	filt = ""
	if filter_vals:
		filt = ["%s='%s'" % (k, v) for k, v in filter_vals.iteritems()]
		filt = " AND ".join(filt)
		filt = "WHERE " + filt
	if key and isinstance(key, (list, tuple)) and len(key) > 0:
		key = ", ".join(key)

	with conn:
		c = conn.cursor()
		plogger.debug("SELECT %s FROM (%s) %s" % (key, table, filt))
		for row in c.execute("SELECT %s FROM (%s) %s" % (key, table, filt)):
			out += [row]

	return out


if __name__ == "__main__":
	pass
	# create()
	# print dump("logs")
	# exists("logs", "foo", 'hu', "log_time")
	# write_log("2", "5", {"user": "0", "log_time": long(1025727)})
	# write_log("2", "7", {"user": "1", "gps_time": long(124025727)})
	# print dump("logs")

	csv = """user,glasses,from_date,to_date
SP:01,M001001C6A,09/03/16,23/05/16
SP:02,M001001A95,09/03/16,23/05/16
SP:04,M001001A95,09/03/16,23/05/16
SP:02,M001001A96,09/03/16,23/05/16
SP:03,M001001742,09/03/16,23/05/16
SP:04,M0010017F9,11/03/16,23/05/16
SP:05,M001001A93,11/03/16,23/05/16
SP:06,M001001C69,11/03/16,23/05/16
SP:07,M001001C86,16/03/16,30/05/16
SP:08,M001001A7B,16/03/16,30/05/16
SP:09,,,
SP:10,,,
SP:11,,,
SP:12,,,
SP:13,,,
SP:14,,,
SP:15,,,
SP:16,,,
SP:17,,,
SP:18,,,
SP:19,,,
SP:20,,,
SP:21,,,
SP:22,,,
SP:23,,,
SP:24,,,
SP:25,,,"""
	open("foo.csv", "w").write(csv)

	csv1 = """user,email,send_until
SP:01,mail@mail,10/04/16
"""
	open("foo1.csv", "w").write(csv1)

	read_csv_to_db_users("foo.csv")
	read_csv_to_db_contacts("foo1.csv")

	user = "SP:01"
	m = read("contacts", ["email", "send_until"], {"user": user})
	print("*", m[0][1], m[0][0])
	print(read("users", "glasses", {"user": "SP:02"}))
	print(read("users", ["from_date", "glasses"], {"user": "SP:02"}))
	print(read("users", "user", {"glasses": "M001001A95"}))
